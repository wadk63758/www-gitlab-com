---
layout: handbook-page-toc
title: "Creating and accessing your account"
description: “Discover how to create and access your GitLab Demo account”
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

To get started with the GitLab Demo Cloud, you will need to sign in to GitLab Demo Portal at [https://gitlabdemo.com](https://gitlabdemo.com) with Okta. After you sign in, you can access your dashboard that provides access to all of the resources in the GitLab Demo Cloud.

This tutorial shows you how to sign in, access the GitLab instance, access integrations, and explore the catalog.

## Step-by-Step Instructions

### Task 1. Creating Your Demo Cloud Account

1. Open a **new tab or window** in your web browser.
2. Visit [https://gitlabdemo.com](https://gitlabdemo.com).
3. Click the **Login with Okta** button and perform the authentication process. 

### Task 2. Accessing the GitLab Instance

When you sign in with Okta for the first time, we automatically provision a user account and organizational group (with owner permissions) on the GitLab instance that we use in our demo environment.

1. If you're not already signed in to the GitLab Demo Portal, visit [https://gitlabdemo.com](https://gitlabdemo.com) and sign in using your Okta account.
2. On the Dashboard, you will see all of the Demo Cloud resources that you have access to.
3. Locate the **GitLab Omnibus (US)** card (bordered section).
    > When you created your account on gitlabdemo.com, your account on the Demo Cloud GitLab instance was automatically created using information and from your Okta profile and a randomly generated password that you should save in 1Password.
4. The URL of the GitLab instance is provided for you to bookmark.
    > It is recommended to add the URL of the GitLab Omnibus instance as a website on your existing 1Password record for gitlabdemo.com.
5. If you click the **GitLab Dashboard** button, a new tab will open with the GitLab dashboard for your user account.
6. If you click the **My Group** button, a new tab will open with the GitLab group that was created for you to create your projects in. You will need to authenticate with the GitLab instance before you will be able to access your group (ex. a `404` error will appear).
    > We have designed our GitLab instance to support easy collaboration for projects that are created in groups. Please consider always creating your project in your group and as a personal project unless necessary.

### Task 3. Accessing Integrations

On the GitLab Demo Portal Dashboard, you will see all of the available integrations for the Demo Cloud below the GitLab Omnibus instance. You can use the provided credentials to access the integration. Please see the tutorials to learn more about how to use the integrations.

[Explore Integration Tutorials](/handbook/customer-success/demo-systems/tutorials/integrations)

## Review

You have successfully created your GitLab Demo Portal account and now have access to all of the GitLab Demo Cloud resources. You can get help with your account in the `#demo-systems` channel on Slack.
