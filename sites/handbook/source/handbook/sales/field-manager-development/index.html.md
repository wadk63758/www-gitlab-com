---
layout: handbook-page-toc
title: "Field Manager Development Program"
description: "The Field Manager Development Program will equip managers with a foundational set of skills & practices for effectively managing remote teams across GitLab’s field organization"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Manager effectiveness is important to GitLab’s continued ability to attract, develop, and retain key talent and deliver scalable, efficient growth. In response, the Corporate L&D, People, and Field Enablement teams are collaborating to launch this FY22 program to equip managers with a foundational set of skills & practices for effectively managing remote teams across GitLab’s field organization.

## Target Audience
All people managers in the GitLab field organization

## Approach
Managers will participate in a series of quarterly training & reinforcement exercises throughout FY22. While there will be formal training elements, a large focus will be on social learning (learning from others) and practical, real-world application to convert knowledge to action.
- **Q1FY22** (2021-03-22): Program Launch & Recruiting Top Talent (virtual)
- **Q2FY22** (Dates TBD): Field Manager Challenge (virtual)
- **Q3FY22** (Dates TBD): Field Manager Summit (in-person at Contribute)

## Topics
This program will leverage content from the [GitLab Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) and address multiple learning objectives supporting the following categories:
1. Recruiting Top Talent
1. What is a High Performing Team?
1. Organizational / Team Health
1. Giving Feedback & Coaching
1. Management Operating Rhythm

## Core Team
- [Carolyn Bednarz](https://about.gitlab.com/company/team/#cbednarz) (People Business Partner, Sales)
- [Josh Zimmerman](https://about.gitlab.com/company/team/#Josh_Zimmerman) (Learning & Development Manager)
- [David Somers](https://about.gitlab.com/company/team/#dcsomers) (Sr. Director, Field Enablement)
- Field Manager delegates
    - [Timm Ideker](https://about.gitlab.com/company/team/#tideker) (Regional Sales Director, Mid-Market, Global)
    - [Robbie Byrne](https://about.gitlab.com/company/team/#RobbieB) (Area Sales Manager, EMEA)
    - [Tom Plumadore](https://about.gitlab.com/company/team/#Plumadore) (Area Sales Manager, ENT AMER Southeast)
    - [Dave Astor](https://about.gitlab.com/company/team/#disastor) (Manager, Solutions Architects, US East) 
    - [Michael Leutz](https://about.gitlab.com/company/team/#mrleutz) (Manager, Technical Account Managers, EMEA)

